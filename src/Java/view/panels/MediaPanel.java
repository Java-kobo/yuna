package view.panels;

import twitter4j.MediaEntity;
import twitter4j.Status;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Mikio on 2015/02/18.
 */
public class MediaPanel extends JPanel {

    private MediaEntity[] mediaEntities;
    private java.util.List<JLabel> mediaLabels;

    public MediaPanel(Status status) {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        setBackground(Color.WHITE);

        mediaEntities = status.getExtendedMediaEntities();

        mediaLabels = new ArrayList<JLabel>();

        if (mediaEntities.length != 0) {
            try {
                for (final MediaEntity mediaEntity : mediaEntities) {
                    if (mediaEntity.getType().equals("photo") || mediaEntity.getType().equals("animated_gif")) {
                        mediaLabels.add(new JLabel(new ImageIcon(new URL(mediaEntity.getMediaURL()))));
                        mediaLabels.get(mediaLabels.size() - 1).setPreferredSize(new Dimension(100, 100));

                        add(mediaLabels.get(mediaLabels.size() - 1));

                        mediaLabels.get(mediaLabels.size() - 1).addMouseListener(new MouseAdapter() {
                            @Override
                            public void mouseClicked(MouseEvent e) {
                                try {
                                    JFrame frame = new JFrame("Media");
                                    frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

                                    // mediaLabelsの中身を直接frameにaddすると、addの先が変わってしまい、
                                    // MediaPanelから消えるため、新しいJLabelのインスタンスを生成して、それをframeに乗せている
                                    JLabel displayLabel = new JLabel(new ImageIcon(new URL(mediaEntity.getMediaURL())));
                                    displayLabel.setPreferredSize(new Dimension(mediaEntity.getSizes().get(2).getWidth(),
                                            mediaEntity.getSizes().get(2).getHeight()));
                                    frame.add(displayLabel);
                                    frame.pack();
                                    frame.setVisible(true);
                                } catch (MalformedURLException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        });
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }
}
