package view.panels;

import model.Credentials;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by Mikio on 2015/02/14.
 */
public class TweetActionPane extends JPanel{

    private FlowLayout layout;

    private JLabel replyLabel;
    private JLabel retweetLabel;
    private JLabel favLabel;

    public TweetActionPane(final Status status) {
        layout = new FlowLayout();
        layout.setVgap(0);
        setLayout(layout);
        setBackground(Color.WHITE);

        replyLabel = new JLabel((new ImageIcon((getClass().getResource("images/reply.png")))));

        if (status.isRetweeted()) {
            retweetLabel = new JLabel(new ImageIcon(getClass().getResource("images/retweet_green.png")));
        } else {
            retweetLabel = new JLabel(new ImageIcon(getClass().getResource("images/retweet_black.png")));
        }

        if (status.isFavorited()) {
            favLabel = new JLabel(new ImageIcon(getClass().getResource("images/star_yellow.png")));
        } else {
            favLabel = new JLabel(new ImageIcon(getClass().getResource("images/star_white.png")));
        }

        replyLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ClientPanel.getUpdateStatusArea().setText("@" + status.getUser().getScreenName() + " ");
                ClientPanel.putReplyScreenName(status.getUser().getScreenName());
                ClientPanel.putReplyTweetId(status.getId());
            }
        });

        retweetLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Status rtStatus = Credentials.getTwitter().showStatus(status.getId());

                    if (!rtStatus.isRetweeted()) {
                        Credentials.getTwitter().retweetStatus(rtStatus.getId());
                        retweetLabel.setIcon(new ImageIcon(getClass().getResource("images/retweet_green.png")));
                    } else {
                        // TODO 直したい
                        ResponseList<Status> tweets = Credentials.getTwitter().getUserTimeline();
                        for (Status tmpStatus : tweets) {
                            if (tmpStatus.isRetweeted() && tmpStatus.getId() == rtStatus.getId()) {
                                Credentials.getTwitter().destroyStatus(tmpStatus.getId());
                                retweetLabel.setIcon(new ImageIcon(getClass().getResource("images/retweet_black.png")));
                                break;
                            }
                        }
                    }
                } catch (TwitterException e1) {
                    e1.printStackTrace();
                }
            }
        });

        favLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Status favStatus = Credentials.getTwitter().showStatus(status.getId());

                    if (!favStatus.isFavorited()) {
                        Credentials.getTwitter().createFavorite(status.getId());
                        favLabel.setIcon(new ImageIcon(getClass().getResource("images/star_yellow.png")));
                    } else {
                        Credentials.getTwitter().destroyFavorite(status.getId());
                        favLabel.setIcon(new ImageIcon(getClass().getResource("images/star_white.png")));
                    }
                } catch (TwitterException e1) {
                    e1.printStackTrace();
                }
            }
        });

        add(replyLabel);
        add(retweetLabel);
        add(favLabel);
    }
}
