package view.panels;

import model.Credentials;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import view.listener.UserStreamListener;
import view.panels.tab.OAuthPanel;
import view.panels.tab.UserStreamPanel;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikio on 2015/01/09.
 */
public class ClientPanel extends JPanel implements Observer {

    private static JTextArea updateStatusArea;
    private static long replyTweetId =0;
    private static String replyScreenName ="";

    private JTabbedPane tabbedPane;

    private UserStreamListener userStreamListener;
    private Credentials credentials;

    public ClientPanel() {
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(500, 600));

        userStreamListener = new UserStreamListener();
        credentials = new Credentials();
        credentials.addObserver(this);
        if (credentials.isAuthorized()) {
            credentials.startUserStream(userStreamListener);
        } else {
            credentials.addUserStreamListener(userStreamListener);
        }

        updateStatusArea = new JTextArea(3, 30);
        updateStatusArea.setLineWrap(true);
        updateStatusArea.setEditable(true);
        updateStatusArea.setBorder(new BevelBorder(BevelBorder.LOWERED));
        add(updateStatusArea, BorderLayout.NORTH);

        tabbedPane = new JTabbedPane();
        add(tabbedPane, BorderLayout.CENTER);

        if (!credentials.isAuthorized()) {
            tabbedPane.addTab("認証", new OAuthPanel(credentials));
        }

        tabbedPane.addTab("Home", new UserStreamPanel(userStreamListener));


        updateStatusArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if ((e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) > 0) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            if(isReplyToStatus(updateStatusArea.getText())){
                                StatusUpdate statusUpdate = new StatusUpdate(updateStatusArea.getText());
                                statusUpdate.setInReplyToStatusId(replyTweetId);
                                credentials.getTwitter().updateStatus(statusUpdate);
                            }else {
                                credentials.getTwitter().updateStatus(updateStatusArea.getText());
                            }
                        } catch (TwitterException e1) {
                            e1.printStackTrace();
                        }
                        updateStatusArea.setText("");
                        initializeReplyScreenName();
                        initializeReplyTweetId();
                    }
                }
            }
        });
    }

    @Override
    public void update(Observable o, Object arg) {
        tabbedPane.remove(0);
    }

    public static JTextArea getUpdateStatusArea() {return updateStatusArea;}

    public static void putReplyTweetId(long replyTweetId){
        ClientPanel.replyTweetId = replyTweetId;
    }

    public static void putReplyScreenName(String replyScreenName){
        ClientPanel.replyScreenName = "@"+replyScreenName;
    }

    private void initializeReplyScreenName() {
        replyScreenName = "";
    }
    private void initializeReplyTweetId() {
        replyTweetId = 0;
    }

    private boolean isReplyToStatus(String Text){
        if(replyScreenName.equals("")){
            return false;
        }
        Pattern pattern = Pattern.compile(replyScreenName);
        Matcher matcher = pattern.matcher(Text);
        return matcher.find();
    }
}