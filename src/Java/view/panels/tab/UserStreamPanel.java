package view.panels.tab;

import model.UserStreamModel;
import view.listener.UserStreamListener;
import view.panels.StatusPanel;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Mikio on 2015/01/11.
 */
public class UserStreamPanel extends JScrollPane implements Observer {

    private SpringLayout layout;
    private JPanel viewPanel;

    private List<StatusPanel> statusPanelList;

    private UserStreamModel model;
    private UserStreamListener userStreamListener;

    public UserStreamPanel(UserStreamListener userStreamListener) {
        layout = new SpringLayout();

        viewPanel = new JPanel();
        viewPanel.setBackground(Color.WHITE);
        viewPanel.setLayout(layout);

        getVerticalScrollBar().setUnitIncrement(10);

        statusPanelList = new ArrayList<StatusPanel>();

        this.userStreamListener = userStreamListener;

        model = new UserStreamModel();
        model.addObserver(this);

        userStreamListener.setTimelineModel(model);

        setViewportView(viewPanel);
    }

    @Override
    public void update(Observable o, Object arg) {
        statusPanelList.add(new StatusPanel(((UserStreamModel)o).getNewestStatus()));
        StatusPanel panel = statusPanelList.get(statusPanelList.size() - 1);

        layout.putConstraint(SpringLayout.NORTH, panel, 0, SpringLayout.NORTH, viewPanel);
        layout.putConstraint(SpringLayout.WEST, panel, 0, SpringLayout.WEST, viewPanel);
        layout.putConstraint(SpringLayout.EAST, panel, 0, SpringLayout.EAST, viewPanel);

        for (int i = statusPanelList.size() - 1; i > 0; i--) {
            layout.putConstraint(SpringLayout.NORTH, statusPanelList.get(i - 1), 0,
                    SpringLayout.SOUTH, statusPanelList.get(i));
        }

        // キメウチなので直したい
        viewPanel.add(panel);
        viewPanel.setPreferredSize(new Dimension(50, getAllStatusPanelHeight() + 75));
        viewPanel.revalidate();
    }

    private int getAllStatusPanelHeight() {
        int height = 0;
        for (int i = 0; i < statusPanelList.size(); i++) {
            height += statusPanelList.get(i).getHeight();
        }
        return height;
    }
}
