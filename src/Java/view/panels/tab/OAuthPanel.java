package view.panels.tab;

import model.Credentials;
import view.listener.AddAccountListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by Mikio on 2015/01/09.
 */
public class OAuthPanel extends JPanel {

    private JLabel label;
    private JLabel urlLabel;
    private JTextField pinField;
    private JButton sendPinButton;

    private Credentials credentials;

    public OAuthPanel(Credentials credentials) {
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        this.credentials = credentials;

        label = new JLabel("URLを開いてPINコード番号を入力し、送信ボタンを押してください");
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(label, gbc);

        // addMouseListenerの時に生成している内部クラスの中でauthorizationURLを呼び出しているので、
        // authorizationURLを後から変更できないようにするためfinalをつける
        final String authorizationURL = credentials.getAuthorizationURL();
        urlLabel = new JLabel("<html><a href=" + authorizationURL + ">" + authorizationURL + "</a></html>");
        gbc.gridx = 0;
        gbc.gridy = 1;
        add(urlLabel, gbc);

        pinField = new JTextField(10);
        gbc.gridx = 0;
        gbc.gridy = 2;
        add(pinField, gbc);

        sendPinButton = new JButton("送信");
        gbc.gridx = 0;
        gbc.gridy = 3;
        add(sendPinButton, gbc);

        urlLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(new URI(authorizationURL));
                } catch (IOException e1) {
                    e1.printStackTrace();
                } catch (URISyntaxException e1) {
                    e1.printStackTrace();
                }
            }
        });

        sendPinButton.addActionListener(new AddAccountListener(pinField, credentials));
    }
}
