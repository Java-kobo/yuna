package view.panels;

import twitter4j.Status;

import javax.swing.*;
import javax.swing.border.MatteBorder;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Mikio on 2015/01/11.
 */
public class StatusPanel extends JPanel {

    private JPanel profileImagePanel;
    private JLabel profileImage;

    private JLabel screenName;
    private JTextPane statusTextPane;
    private TweetActionPane tweetActionPane;
    private MediaPanel mediaPanel;

    public StatusPanel(Status status) {
        setBackground(Color.WHITE);
        setBorder(new MatteBorder(new Insets(0, 0, 1, 0), Color.BLACK));
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        // profile
        profileImagePanel = new JPanel();
        profileImagePanel.setLayout(new BorderLayout());
        try {
            if (status.isRetweet()) {
                profileImage = new JLabel(new ImageIcon(new URL(status.getRetweetedStatus().getUser().getProfileImageURL())));
            } else {
                profileImage = new JLabel(new ImageIcon(new URL(status.getUser().getProfileImageURL())));
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        profileImage.setPreferredSize(new Dimension(50, 50));
        profileImagePanel.add(profileImage, BorderLayout.CENTER);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridheight = 2;
        gbc.weightx = 0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.NORTH;
        add(profileImagePanel, gbc);

        // RT時
        if (status.isRetweet()) {
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.gridheight = 1;
            gbc.weightx = 0;
            gbc.fill = GridBagConstraints.NONE;
            gbc.anchor = GridBagConstraints.EAST;
            add(new JLabel(new ImageIcon(getClass().getResource("images/retweet_gray.png"))), gbc);

            gbc.gridx = 1;
            gbc.gridy = 0;
            gbc.gridheight = 1;
            gbc.weightx = 0;
            gbc.fill = GridBagConstraints.HORIZONTAL;
            gbc.anchor = GridBagConstraints.WEST;
            add(new JLabel(status.getUser().getName() + "がRTしました"), gbc);
        }


        // UserName and ScreenName
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        gbc.weightx = 0;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.WEST;
        if (status.isRetweet()) {

            screenName = new JLabel("<html><body style=\"font-family: " + (String)getFont().getFamily() + "\">"
                    + "<a href=\""+ "@" + status.getRetweetedStatus().getUser().getScreenName() + "\">"
                    + status.getRetweetedStatus().getUser().getName() + "</a>"
                    + " @" + status.getRetweetedStatus().getUser().getScreenName() + "</body></html>");
        } else {
            screenName = new JLabel("<html><body style=\"font-family: " + (String)getFont().getFamily() + "\">"
                    + "<a href=\""+ "@" + status.getUser().getScreenName() + "\">"
                    + status.getUser().getName() + "</a>"
                    + " @" + status.getUser().getScreenName() + "</body></html>");
        }
        add(screenName, gbc);

        // ツイート本文
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        gbc.weightx = 1.0d;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.anchor = GridBagConstraints.NORTH;
        statusTextPane = new JTextPane();
        statusTextPane.setContentType("text/html");
        statusTextPane.setEditable(false);
        if (status.isRetweet()) {
            statusTextPane.setText("<html><body style=\"font-family: " + (String)getFont().getFamily() + "\">"
                    + urlToHTML(status.getRetweetedStatus().getText()) + "</body></html>");
        } else {
            statusTextPane.setText("<html><body style=\"font-family: " + (String)getFont().getFamily() + "\">"
                    + urlToHTML(status.getText()) + "</html>");
        }
        statusTextPane.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    try {
                        Desktop.getDesktop().browse(e.getURL().toURI());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (URISyntaxException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });
        add(statusTextPane, gbc);

        // 画像等の表示パネル
        gbc.gridx = 1;
        gbc.gridy = 3;
        gbc.gridheight = 1;
        gbc.weightx = 0;
        gbc.weighty = 1.0d;
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.CENTER;
        mediaPanel = new MediaPanel(status);
        add(mediaPanel, gbc);

        // fav, RTのボタン
        gbc.gridx = 1;
        gbc.gridy = 4;
        gbc.gridheight = 1;
        gbc.weightx = 1.0d;
        gbc.weighty = 0;
        gbc.fill = GridBagConstraints.NONE;
        gbc.anchor = GridBagConstraints.CENTER;
        tweetActionPane = new TweetActionPane(status);
        add(tweetActionPane, gbc);
    }

    private String urlToHTML(String string) {
        string = string.replaceAll("\n|\r\n|\n\r","<br>");
        Pattern urlLink = Pattern.compile("(http://|https://){1}[\\w\\.\\-/:\\#\\?\\=\\&\\;\\%\\~\\+]+",
                Pattern.CASE_INSENSITIVE);
        Matcher matcher = urlLink.matcher(string);
        return matcher.replaceAll("<a href=\"$0\">$0</a>");
    }
}
