package view.listener;

import model.Credentials;
import model.MentionModel;
import model.UserStreamModel;
import twitter4j.*;

/**
 * Created by Mikio on 2015/01/10.
 */
public class UserStreamListener implements twitter4j.UserStreamListener {

    private UserStreamModel model;
    private MentionModel mentionModel;

    public UserStreamListener() {}

    public void setTimelineModel(UserStreamModel model) {
        this.model = model;
    }

    public void setMentionModel(MentionModel mentionModel) {
        this.mentionModel = mentionModel;
    }

    @Override
    public void onDeletionNotice(long l, long l1) {

    }

    @Override
    public void onFriendList(long[] longs) {

    }

    @Override
    public void onFavorite(User user, User user1, Status status) {

    }

    @Override
    public void onUnfavorite(User user, User user1, Status status) {

    }

    @Override
    public void onFollow(User user, User user1) {

    }

    @Override
    public void onUnfollow(User user, User user1) {

    }

    @Override
    public void onDirectMessage(DirectMessage directMessage) {

    }

    @Override
    public void onUserListMemberAddition(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListMemberDeletion(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListSubscription(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListUnsubscription(User user, User user1, UserList userList) {

    }

    @Override
    public void onUserListCreation(User user, UserList userList) {

    }

    @Override
    public void onUserListUpdate(User user, UserList userList) {

    }

    @Override
    public void onUserListDeletion(User user, UserList userList) {

    }

    @Override
    public void onUserProfileUpdate(User user) {

    }

    @Override
    public void onBlock(User user, User user1) {

    }

    @Override
    public void onUnblock(User user, User user1) {

    }

    @Override
    public void onStatus(Status status) {
        model.addStatus(status);
        try {
            if ((status.getInReplyToUserId() == Credentials.getTwitter().getId()) && !status.isRetweet()) {
                mentionModel.addStatus(status);
            }
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {

    }

    @Override
    public void onTrackLimitationNotice(int i) {

    }

    @Override
    public void onScrubGeo(long l, long l1) {

    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {

    }

    @Override
    public void onException(Exception e) {

    }
}
