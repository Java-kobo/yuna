package view.listener;

import model.Credentials;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mikio on 2015/01/09.
 */
public class AddAccountListener implements ActionListener {

    private JTextField pinField;
    private Credentials credentials;

    public AddAccountListener(JTextField pinField, Credentials credentials) {
        this.pinField = pinField;
        this.credentials = credentials;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        credentials.getAccessToken(pinField.getText());
    }
}
