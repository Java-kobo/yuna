package view;

import view.panels.ClientPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Mikio on 2015/01/09.
 */
public class MainFrame extends JFrame {

    public MainFrame() {
        setTitle("ぷろとたいぷ");
        setLayout(new BorderLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Container contentPane = getContentPane();
        contentPane.add(new ClientPanel(), BorderLayout.CENTER);
        setMinimumSize(new Dimension(400, 300));

        pack();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
}
