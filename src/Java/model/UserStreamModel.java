package model;

import twitter4j.Status;

import java.util.LinkedList;
import java.util.Observable;

/**
 * Created by Mikio on 2015/02/08.
 */
public class UserStreamModel extends Observable {

    private LinkedList<Status> statuses;

    public UserStreamModel() {
        statuses = new LinkedList<Status>();
    }

    public Status getNewestStatus() {
        return statuses.get(statuses.size() - 1);
    }

    public LinkedList<Status> getStatuses() {
        return statuses;
    }

    public void addStatus(Status status) {
        statuses.addLast(status);
        setChanged();
        notifyObservers();
    }
}
