package model;

import twitter4j.Status;
import twitter4j.TwitterException;

import java.util.LinkedList;
import java.util.List;
import java.util.Observable;

/**
 * Created by Mikio on 2015/02/08.
 */
public class MentionModel extends Observable {

    private LinkedList<Status> statuses;

    public MentionModel() {
        statuses = new LinkedList<Status>();
    }

    public void loadStatuses() {
        try {
            List<Status> tempList = Credentials.getTwitter().getMentionsTimeline();
            for (Status status : tempList) {
                statuses.add(status);
            }
        } catch (TwitterException e) {
            e.printStackTrace();
        }
        setChanged();
        notifyObservers();
    }

    public LinkedList<Status> getStatuses() {
        return statuses;
    }

    public void addStatus(Status status) {
        statuses.addLast(status);
        setChanged();
        notifyObservers();
    }
}
