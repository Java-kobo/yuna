# 認証の流れ(認証ですること)
****

 * Twitterのインスタンスを作る
 * RequestTokenを取得する
 * PINを取得するためのURLを生成
 * URLで認証をしてPINを入力させる
 * AccessTokenを取得
 * twitter4j.propertiesにAccessTokenを保存


# Twitterクライアントがやっていること
****

 * Twitterのアカウントを登録すること
 * アカウントのタイムラインを表示すること
 * アカウントのツイートを投稿すること(fav, RT, )